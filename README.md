# Lib for consumer and producer messages to Node.js in AWS SQS

## How use

 - Installation

 ```
 npm install --save aws-sqs-consumer-and-producer
 ```

- Producer

```
const { Producer } = require('aws-sqs-consumer-and-producer')

new Producer({
    region: "xxx",
    accessKeyId: "xxx",
    secretAccessKey: "xxx"
}).publishMessage('queueUrl', 'payload', (err, data) => {
    if (err) {
        console.log("Error", err)
    } else {
        console.log("Success", data.MessageId)
    }
})
```

- Consumer

```
const { Consumer } = require('aws-sqs-consumer-and-producer')

new Consumer({
        region: "xxx",
        accessKeyId: "xxx",
        secretAccessKey: "xxx"
    },
    'queueUrl')
    .startConsumer((err, data) => {
    if (err) {
        console.log("Receive error: ", err)
    } else {
        console.log(data.Messages[0].Body)
    }
})
```

## Author
- [Marcelo Viana](https://www.linkedin.com/in/marcelovianaalmeida/)