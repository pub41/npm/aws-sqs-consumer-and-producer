const AWS = require('aws-sdk');

class Producer {

  constructor (awsCredentials = {
    region: 'us-east-1'
  }) {
    AWS.config.update(awsCredentials)
    this.sqs = new AWS.SQS({apiVersion: '2012-11-05'})
  }

  publishMessage = async (queueUrl = null, payload = {}, callback) => {
    
    let params = {
      MessageBody: typeof payload === 'string' ? payload: JSON.stringify(payload),
      QueueUrl: queueUrl
    }
  
    if (!queueUrl) {
      callback({message: 'The QueueUrl cannot be null'})
    }
  
    await this.sqs.sendMessage(params, function(err, data) {
      callback(err, data)
    })
  }
}


module.exports = Producer