const AWS = require('aws-sdk')

class Consumer {

    constructor(awsCredentials = {
      region: 'us-east-1'
    }, urlQueue) {
      AWS.config.update(awsCredentials)
      this.sqs = new AWS.SQS({apiVersion: '2012-11-05'})      
      this.urlQueue = urlQueue
    }

    deleteMessange (deleteParams) {
      this.sqs.deleteMessage(deleteParams, function(err, data) {
        if (err) {
          console.log("Delete Error", err)
        } else {
          console.log('Deleted message', data)
        }
      })
    }
    
    startConsumer = (callback) => {

      if (!this.urlQueue || typeof this.urlQueue !== 'string') {
        callback('undefined queueUrl or is not string')
        return
      }

      let params = {
        AttributeNames: [
           "SentTimestamp"
        ],
        MaxNumberOfMessages: 10,
        MessageAttributeNames: [
           "All"
        ],
        QueueUrl: this.urlQueue,
        VisibilityTimeout: 20,
        WaitTimeSeconds: 0
       }

        setInterval((vm = this) => {
          this.sqs.receiveMessage(params, function(err, data) {
            if (err) {
              callback(err)
            } else if (data.Messages) {
              let deleteParams = {
                QueueUrl: params.QueueUrl,
                ReceiptHandle: data.Messages[0].ReceiptHandle
              }
              vm.deleteMessange(deleteParams)
              callback(false, data.Messages[0].Body)
            }
          })
        }, 1000)

    }

}

module.exports = Consumer
