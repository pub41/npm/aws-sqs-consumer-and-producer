const Producer = require('./modules/aws-sqs-producer')
const Consumer = require('./modules/aws-sqs-consumer')

module.exports = {
    Producer,
    Consumer
}